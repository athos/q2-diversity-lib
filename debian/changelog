q2-diversity-lib (2024.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Regenerate debian/control from debian/control.in (routine-update)
  * Autopkgtest for all supported Python3 versions

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2024 14:40:12 +0100

q2-diversity-lib (2023.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Generate debian/control automatically to refresh version number
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Regenerate debian/control from debian/control.in (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 30 Jan 2024 16:23:13 +0100

q2-diversity-lib (2023.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Étienne Mollier <emollier@debian.org>  Sun, 20 Aug 2023 14:35:31 +0200

q2-diversity-lib (2022.11.1-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Andreas Tille <tille@debian.org>  Tue, 24 Jan 2023 18:46:15 +0100

q2-diversity-lib (2022.11.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Bump versioned Depends

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2023 12:35:17 +0100

q2-diversity-lib (2022.8.0-3) unstable; urgency=high

  * Team Upload.
  * Add Depends on python3-sklearn
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Fri, 06 Jan 2023 08:30:25 +0530

q2-diversity-lib (2022.8.0-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Depends: s/libssu-tools/unifrac-tools/
    Closes: #1021542
  * Versioned (Build-)Depends: python3-unifrac (>= 1.1.1)

 -- Nilesh Patra <nilesh@debian.org>  Tue, 27 Dec 2022 12:07:19 +0530

q2-diversity-lib (2022.8.0-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 2022.8.0
  * Updated dependency on qiime/q2-* >= 2022.8.0
  * Replace B-D on nose with pytest (Closes: #1018607)
  * Depends on libssu-tools

 -- Nilesh Patra <nilesh@debian.org>  Tue, 06 Sep 2022 18:05:20 +0530

q2-diversity-lib (2021.8.0-2) unstable; urgency=medium

  * Team upload.
  * Hint to /usr/share/common-licenses/CC0-1.0

 -- Andreas Tille <tille@debian.org>  Wed, 17 Nov 2021 12:10:54 +0100

q2-diversity-lib (2021.8.0-1) unstable; urgency=medium

  * Team upload.
  * Initial release (Closes: #992104).

 -- Andreas Tille <tille@debian.org>  Tue, 16 Nov 2021 08:15:08 +0100
